FROM python:3.8-alpine

# ENV
# ENV FLASK_APP app.py
# ENV FLASK_RUN_HOST 0.0.0.0

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
COPY requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

# Bundle app source
COPY . .

# EXPOSE 8080
# CMD [ "python", "./aqi.py" ]

CMD ["python", "-u", "app.py", "influxdb", "8086"]